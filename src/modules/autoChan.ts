import {
    ChannelType,
    NewsChannel,
    PermissionsBitField,
    SlashCommandBuilder,
    TextChannel,
} from "discord.js";
import WTFIXClient, { BotModule } from "../wtfixclient";
import winston from "winston";

const AutoChanModule: BotModule = {
    commands: [
        {
            data: new SlashCommandBuilder()
                .setName("setautochan")
                .setDescription(
                    "Set the channel where xur location auto posts will be sent. (Admin)",
                )
                .setDMPermission(false)
                .setDefaultMemberPermissions(
                    PermissionsBitField.Flags.Administrator,
                )
                .addChannelOption((option) =>
                    option
                        .setName("channel")
                        .setDescription(
                            "Channel where auto posts will be posted.",
                        )
                        .setRequired(true)
                        .addChannelTypes(
                            ChannelType.GuildText,
                            ChannelType.GuildAnnouncement,
                        ),
                ),
            async execute(interaction) {
                if (!interaction.guild) {
                    await interaction.reply({
                        content: "This command only works in servers!",
                    });
                }
                if (interaction.member === null) {
                    throw new Error("Interaction member is null!");
                }
                if (
                    !interaction.member.permissions.has(
                        PermissionsBitField.Flags.Administrator,
                    )
                ) {
                    await interaction.reply({
                        content: "Only administrators can use this command!",
                        ephemeral: true,
                    });
                    return;
                }

                if (interaction.guild === null) {
                    throw new Error("Interaction guild is null!");
                }

                const chan = interaction.options.getChannel("channel") as
                    | TextChannel
                    | NewsChannel
                    | null;
                if (chan) {
                    if (chan.guild.members.me === null) {
                        throw new Error(
                            "Interaction guild members.me is null!",
                        );
                    }
                    if (
                        chan.viewable &&
                        chan
                            .permissionsFor(chan.guild.members.me)
                            .has([
                                PermissionsBitField.Flags.SendMessages,
                                PermissionsBitField.Flags.EmbedLinks,
                            ])
                    ) {
                        await (interaction.client as WTFIXClient).provider.set(
                            interaction.guild,
                            "updateChan",
                            chan.id,
                        );
                        await interaction.reply({
                            content: `Set the xur location update channel to be \`${chan.name}\`!`,
                            ephemeral: true,
                        });
                    } else {
                        winston.log(
                            "info",
                            "Invalid posting permissions for channel",
                        );
                        await interaction.reply({
                            content: `I don't have permissions to post locations in that channel, please make sure I have the "Send Messages" and "Embed Links" permission in that channel.`,
                            ephemeral: true,
                        });
                    }
                } else {
                    throw new Error("Can't find channel for setAutoChan.");
                }
            },
        },
        {
            data: new SlashCommandBuilder()
                .setName("disableautochan")
                .setDescription(
                    "Disable the xur location auto posts in the server. (Admin)",
                )
                .setDMPermission(false)
                .setDefaultMemberPermissions(
                    PermissionsBitField.Flags.Administrator,
                ),
            async execute(interaction) {
                if (!interaction.guild) {
                    await interaction.reply({
                        content: "This command only works in servers!",
                    });
                }
                if (interaction.member === null) {
                    throw new Error("Interaction member is null!");
                }
                if (
                    !interaction.member.permissions.has(
                        PermissionsBitField.Flags.Administrator,
                    )
                ) {
                    await interaction.reply({
                        content: "Only administrators can use this command!",
                        ephemeral: true,
                    });
                    return;
                }

                if (interaction.guild === null) {
                    throw new Error("Interaction guild is null!");
                }

                await (interaction.client as WTFIXClient).provider.remove(
                    interaction.guild,
                    "updateChan",
                );
                await interaction.reply({
                    content: "Removed the xur auto posts from the server.",
                    ephemeral: true,
                });
            },
        },
        {
            data: new SlashCommandBuilder()
                .setName("checkautochan")
                .setDescription(
                    "Check which channel is currently registered to receive xur auto posts (Admin)",
                )
                .setDMPermission(false)
                .setDefaultMemberPermissions(
                    PermissionsBitField.Flags.Administrator,
                ),
            async execute(interaction) {
                if (!interaction.guild) {
                    await interaction.reply({
                        content: "This command only works in servers!",
                    });
                }
                if (interaction.member === null) {
                    throw new Error("Interaction member is null!");
                }
                if (
                    !interaction.member.permissions.has(
                        PermissionsBitField.Flags.Administrator,
                    )
                ) {
                    await interaction.reply({
                        content: "Only administrators can use this command!",
                        ephemeral: true,
                    });
                    return;
                }

                if (interaction.guild === null) {
                    throw new Error("Interaction guild is null!");
                }

                const updateChan = await (
                    interaction.client as WTFIXClient
                ).provider.get(interaction.guild, "updateChan", null);

                if (updateChan) {
                    await interaction.reply({
                        content: `The auto post channel in this server is set to <#${updateChan}>`,
                        ephemeral: true,
                    });
                } else {
                    await interaction.reply({
                        content: `Auto post is not configured in this server, use /setautochan to set it up!`,
                        ephemeral: true,
                    });
                }
            },
        },
    ],
};

export default AutoChanModule;
