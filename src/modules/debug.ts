import {
    SlashCommandBuilder,
    ModalBuilder,
    TextInputBuilder,
    TextInputStyle,
    ActionRowBuilder,
    BaseInteraction,
    ChatInputCommandInteraction,
} from "discord.js";
import WTFIXClient, { BotModule } from "../wtfixclient";
import isXurHere from "../utils/isXurHere";
import { Converter } from "showdown";
import { XurUpdate } from "../utils/buildXurEmbed";

const converter = new Converter();

const DebugModule: BotModule = {
    isDebug: true,
    commands: [
        {
            data: new SlashCommandBuilder()
                .setName("forceautopost")
                .setDescription("Force auto xur posts when the bot fucks up")
                .addIntegerOption((option) =>
                    option
                        .setName("shardid")
                        .setDescription("Shard ID to force auto posts on"),
                ),
            async execute(interaction) {
                if (
                    interaction.user.id != "137429565063692289" &&
                    interaction.user.id != "226205409235435522"
                ) {
                    await interaction.reply({
                        content:
                            "You are not a bot owner, you cannot use this!",
                    });
                }

                const shardid = interaction.options.getInteger("shardid");

                let shardsToForce;
                if (shardid == null) {
                    shardsToForce = [
                        ...Array(interaction.client.shard?.count ?? 0).keys(),
                    ];
                } else {
                    if (
                        shardid < 0 ||
                        shardid > (interaction.client.shard?.count ?? 0) - 1
                    ) {
                        await interaction.reply({
                            content: "Invalid shard id",
                        });
                        return;
                    }
                    shardsToForce = [shardid];
                }

                await interaction.reply({
                    content: `Running on shards ${shardsToForce.join()}...`,
                });
                // for (const shard of shardsToForce) {
                //     interaction.client.shard?.broadcastEval(
                //         (client) => (client as WTFIXClient).xurAutoChan(),
                //         {
                //             shard: shard,
                //         },
                //     );
                // }
            },
        },
        {
            data: new SlashCommandBuilder()
                .setName("stats")
                .setDescription(
                    "OWNER ONLY, IF YOU CAN SEE THIS AND YOU AREN'T AN OWNER PLEASE CONTACT A WTFIX ADMIN",
                ),
            async execute(interaction) {
                if (
                    interaction.user.id != "137429565063692289" &&
                    interaction.user.id != "226205409235435522"
                ) {
                    await interaction.reply({
                        content:
                            "You are not a bot owner, you cannot use this!",
                    });
                }

                const shardGuildCounts: number[] =
                    ((await interaction.client.shard?.fetchClientValues(
                        "guilds.cache.size",
                    )) ?? []) as number[];
                const totalGuilds = shardGuildCounts.reduce(
                    (total: number, count: number) => total + count,
                    0,
                );
                const shardMemberCounts =
                    (await interaction.client.shard?.broadcastEval((client) =>
                        client.guilds.cache.reduce(
                            (total, guild) => total + guild.memberCount,
                            0,
                        ),
                    )) ?? [];
                const totalUsers = shardMemberCounts.reduce(
                    (total: number, count: number) => total + count,
                    0,
                );
                await interaction.reply({
                    content: `Bot has ${interaction.client.shard?.count} shards in ${totalGuilds} servers and services ${totalUsers} users.`,
                });
            },
        },
        {
            data: new SlashCommandBuilder()
                .setName("xurcustom")
                .setDescription(
                    "Set xur's location with custom properties, in case xur is in a new location",
                )
                .addStringOption((option) =>
                    option
                        .setName("planet")
                        .setDescription("The planet xur is on")
                        .setRequired(true),
                )
                .addStringOption((option) =>
                    option
                        .setName("zone")
                        .setDescription("The zone xur is in")
                        .setRequired(true),
                )
                .addStringOption((option) =>
                    option
                        .setName("desc")
                        .setDescription(
                            "A brief (and funny) description of where xur is in the zone",
                        )
                        .setRequired(true),
                ),
            async execute(interaction) {
                const currentTime = new Date();
                if (isXurHere(currentTime)) {
                    const planet = interaction.options.getString("planet");
                    const zone = interaction.options.getString("zone");
                    const desc = interaction.options.getString("desc");
                    const newXur = {
                        location: {
                            planet: planet,
                            zone: zone,
                            desc: desc,
                        },
                        present: true,
                        lastUpdate: currentTime.toISOString(),
                    };
                    await (interaction.client as WTFIXClient).redisdb.json.set(
                        "wtfix:xur",
                        "$",
                        newXur,
                    );
                    await (interaction.client as WTFIXClient).redisdb.publish(
                        "xur",
                        "",
                    );
                    await refreshSite();
                    await interaction.reply({ content: "Updated!" });
                } else {
                    await interaction.reply({
                        content:
                            "Xur isn't here, so you can't set his location!",
                    });
                }
            },
        },
        {
            data: new SlashCommandBuilder()
                .setName("xurimg")
                .setDescription("Set the map to use to show xur's location")
                .addStringOption((option) =>
                    option
                        .setName("img")
                        .setDescription("The URL of the img to use")
                        .setRequired(true),
                ),
            async execute(interaction) {
                const currentTime = new Date();
                if (isXurHere(currentTime)) {
                    const xur = (await (
                        interaction.client as WTFIXClient
                    ).redisdb.json.get("wtfix:xur", {
                        path: ".",
                    })) as XurUpdate;
                    if (xur.present == true && xur.location !== undefined) {
                        if (xur.location === undefined) {
                            await interaction.reply({
                                content:
                                    "Please set xur's location first, before setting a map!",
                            });
                        } else {
                            xur.location.customMap =
                                interaction.options.getString("img") ??
                                undefined;
                            await (
                                interaction.client as WTFIXClient
                            ).redisdb.json.set("wtfix:xur", "$", xur);
                            await (
                                interaction.client as WTFIXClient
                            ).redisdb.publish("xur", "");
                            await refreshSite();
                            await interaction.reply({ content: "Updated!" });
                        }
                    }
                } else {
                    await interaction.reply({
                        content: "Xur isn't here, so you can't set his image!",
                    });
                }
            },
        },
        {
            data: new SlashCommandBuilder()
                .setName("getloginlink")
                .setDescription(
                    "Sends the link to login to retrieve your refresh token.",
                ),
            async execute(interaction) {
                await interaction.reply({
                    content: `https://www.bungie.net/en/oauth/authorize?client_id=${process.env.BUNGIE_API_ID}&response_type=code`,
                });
            },
        },
        {
            data: new SlashCommandBuilder()
                .setName("xurmsg")
                .setDescription("Set the xur msg on the homepage"),
            async execute(interaction) {
                await setText("xurmsg", interaction);
            },
        },
        {
            data: new SlashCommandBuilder()
                .setName("psa")
                .setDescription("Set the psa on the homepage"),
            async execute(interaction) {
                await setText("psa", interaction);
            },
        },
        {
            data: new SlashCommandBuilder()
                .setName("riff")
                .setDescription("Set the riff on the homepage"),
            async execute(interaction) {
                await setText("riff", interaction);
            },
        },
        {
            data: new SlashCommandBuilder()
                .setName("exec")
                .setDescription(
                    "OWNER ONLY, IF YOU CAN SEE THIS AND YOU AREN'T AN OWNER PLEASE CONTACT A WTFIX ADMIN",
                )
                .addStringOption((option) =>
                    option
                        .setName("snippet")
                        .setDescription("The code to execute")
                        .setRequired(true),
                ),
            async execute(interaction) {
                if (
                    interaction.user.id != "137429565063692289" &&
                    interaction.user.id != "226205409235435522"
                ) {
                    await interaction.reply({
                        content:
                            "You are not a bot owner, you cannot use this!",
                    });
                }

                const snippet = interaction.options.getString("snippet");
                if (snippet == null) {
                    await interaction.reply({ content: "Snippet is null" });
                    return;
                }

                const resps =
                    (await interaction.client.shard?.broadcastEval(
                        (client, ctx: { snippet: string }) => {
                            const resp = new Function("client", ctx.snippet)(
                                client,
                            );
                            return resp
                                ? [client.shard?.ids, resp]
                                : [client.shard?.ids, null];
                        },
                        { context: { snippet: snippet } },
                    )) ?? [];

                let summaryResp = "";
                for (const resp of resps) {
                    summaryResp += `Shard ${resp[0]}: ${resp[1]}\n`;
                }
                await interaction.reply({ content: summaryResp });
            },
        },
    ],
    events: [
        {
            name: "interactionCreate",
            listener: async (interaction: BaseInteraction) => {
                if (!interaction.isModalSubmit()) return;
                const ids = interaction.customId.split(":");
                if (ids.length > 1 && ids[0] == "wtfixtext") {
                    const rawText =
                        interaction.fields.getTextInputValue("textInput");

                    const text = rawText.replace(/\\n/g, "\n");
                    const html = converter.makeHtml(text);
                    const exists = await (
                        interaction.client as WTFIXClient
                    ).redisdb.exists("wtfix:msg");
                    if (!exists) {
                        await (
                            interaction.client as WTFIXClient
                        ).redisdb.json.set("wtfix:msg", "$", {});
                    }
                    await (interaction.client as WTFIXClient).redisdb.json.set(
                        "wtfix:msg",
                        `$.${ids[1]}`,
                        html,
                    );
                    await (interaction.client as WTFIXClient).redisdb.publish(
                        "msg",
                        "",
                    );
                    await refreshSite();
                    await interaction.reply({
                        content: `Updated the ${ids[1]} to say \`\`\`${html}\`\`\`\nOriginal text:\`\`\`${rawText}\`\`\``,
                    });
                }
            },
        },
    ],
};

export default DebugModule;

async function refreshSite() {
    if (process.env.API_KEY == undefined) {
        return;
    }
    await fetch(
        "http://wtfix.gg/api/refresh?" +
            new URLSearchParams({
                key: process.env.API_KEY,
            }),
        {
            method: "GET",
        },
    );
}

async function setText(key: string, interaction: ChatInputCommandInteraction) {
    const modal = new ModalBuilder()
        .setCustomId(`wtfixtext:${key}`)
        .setTitle(`Set new ${key} message`);

    const textInput = new TextInputBuilder()
        .setCustomId("textInput")
        .setLabel(`Enter a new message for the ${key} box.`)
        .setStyle(TextInputStyle.Paragraph);

    const actionRow = new ActionRowBuilder<TextInputBuilder>().addComponents(
        textInput,
    );
    modal.addComponents(actionRow);

    await interaction.showModal(modal);
}
