import pg from "pg";
import discord, { Guild } from "discord.js";
import WTFIXClient, { ConfigProvider } from "./wtfixclient";

/**
 * Uses an Postgres database to store settings with guilds
 * @extends {SettingProvider}
 */
export class PostgresProvider implements ConfigProvider {
	settings: Map<string, Record<string, string | number | undefined>>;
	constructor(private db: pg.Client, private client: discord.Client) {
		/**
		 * Settings cached in memory, mapped by guild ID (or 'global')
		 * @type {Map}
		 * @private
		 */
		this.settings = new Map();
	}

	async init() {
		await this.db.query('CREATE TABLE IF NOT EXISTS bot_settings (guild bigint PRIMARY KEY, settings text)');

		// Load all settings
		const resp = await this.db.query('SELECT CAST(guild as TEXT) as guild, settings FROM bot_settings');
		for(const row of resp.rows) {
			let settings;
			try {
				settings = JSON.parse(row.settings);
			} catch(err) {
				this.client.emit('warn', `PostgresProvider couldn't parse the settings stored for guild ${row.guild}.`);
				continue;
			}

			const guild = row.guild !== '0' ? row.guild : 'global';
			this.settings.set(guild, settings);
			if(guild !== 'global' && !this.client.guilds.cache.has(row.guild)) continue;
		}
	}

	async destroy() {
		
	}

	async get<T>(guild: string | Guild, key: string, defVal: T): Promise<T> {
		const settings = this.settings.get(getGuildID(guild));
		return settings ? typeof settings[key] !== 'undefined' ? settings[key] as T : defVal : defVal;
	}

	async set(guild: string | Guild, key: string, val: string | number): Promise<void> {
		guild = getGuildID(guild);
		let settings = this.settings.get(guild);
		if(!settings) {
			settings = {};
			this.settings.set(guild, settings);
		}

		settings[key] = val;
		await this.db.query('INSERT INTO bot_settings (guild, settings) VALUES($1, $2) ON CONFLICT (guild) DO UPDATE SET settings = excluded.settings', [guild !== 'global' ? guild : 0, JSON.stringify(settings)]);
		if(guild === 'global') this.updateAllShards(key, val);
	}

	async remove(guild: string | Guild, key: string) {
		guild = getGuildID(guild);
		if(guild === 'global') this.updateAllShards(key, undefined);
		const settings = this.settings.get(guild);
		if(!settings || typeof settings[key] === 'undefined') return;

		settings[key] = undefined;
		await this.db.query('INSERT INTO bot_settings (guild, settings) VALUES($1, $2) ON CONFLICT (guild) DO UPDATE SET settings = excluded.settings', [guild !== 'global' ? guild : 0, JSON.stringify(settings)]);
	}

	async clear(guild: string | Guild) {
		guild = getGuildID(guild);
		if(!this.settings.has(guild)) return;
		this.settings.delete(guild);
		await this.db.query('DELETE FROM bot_settings WHERE guild = $1', [guild !== 'global' ? guild : 0]);
	}

	updateAllShards(key: string, val: string | number | undefined) {
		if(!this.client.shard) return;
		this.client.shard.broadcastEval((client) => {
			const wtfixClient = client as WTFIXClient;
			if (val === undefined) {
				wtfixClient.provider.remove('global', key);
			} else {
				wtfixClient.provider.set('global', key, val);
			}
		});
	}
}

function getGuildID(guild: string | Guild) {
	if(guild instanceof Guild) return guild.id;
	if(typeof guild === 'string' && !Number.isNaN(parseInt(guild))) return guild;
	if(guild === 'global') return 'global';
	throw new TypeError('Invalid guild specified. Must be a Guild instance, guild ID, or "global".');
}