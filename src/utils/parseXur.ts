export default function parseXur(xur: XurLocation) {
    const xurText = xur.planet + ' > ' + xur.zone + ' > ' + xur.desc;
    return xurText;
}

export type XurLocation = {
    planet: string,
    zone: string,
    desc: string,
    customMap?: string,
    maps?: {
        bot: Record<string, string>
        featured: string
    }
}