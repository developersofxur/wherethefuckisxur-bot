export default function isXurHere(time: Date) {
    const day = time.getUTCDay();
    const hour = time.getUTCHours();
    if (day < 2 || day > 5) {
        return true;
    } else if (day == 2) {
        return hour < 17 ? true : false;
    } else if (day == 5) {
        return hour >= 17 ? true : false;
    } else {
        return false;
    }
}