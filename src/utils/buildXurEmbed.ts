import { APIEmbed } from "discord.js";
import { XurLocation } from "./parseXur";

export default function buildXurEmbed(xur: XurUpdate): APIEmbed {
    const embed: APIEmbed = {
        "author": {
            "name": "Where The Fuck Is Xur?",
            "url": process.env["SERVER_HOSTNAME"]
        },
        "color": 14673125,
        "thumbnail": {
            "url": process.env["SERVER_HOSTNAME"] + "/images/logo.512.png"
        },
        "footer": {
            "text": "With love, the WTFIX team. ❤️"
        }
    };
    if (xur.present) {
        if (xur.location) {
            //embed['title'] = parseXur(xur.location);
            embed['title'] = "He's just in the fuckin tower forever now, to the right of the ramen shop, whatever";
            let map = "/images/maps/unknown.png";
            //if (xur.location.customMap) {
            //    map = xur.location.customMap;
            //} else if (xur.location.maps) {
            //    map = xur.location.maps.bot[xur.location.maps.featured];
            //}
            map = "/images/maps/doodle/new_tower.png";
            embed['image'] = {
                'url': process.env["SERVER_HOSTNAME"] + map
            }
        } else {
            embed['title'] = `Xûr's here but we haven't found him yet.  Check back in a few minutes!`;
        }
    } else {
        embed['title'] = `Xûr's fucked off`;
    }
    return embed;
}

export type XurUpdate = {
    location?: XurLocation,
    present: boolean,
}
