const daysToMS = 24 * 60 * 60 * 1000

export default function isSameVisit(previousTime: number, newTime: number) {
    if (newTime - previousTime > 4 * daysToMS) {
        return false;
    } else if (newTime - previousTime > 3 * daysToMS) {
        if ((new Date(previousTime)).getUTCDay() < 3) {
            return false
        } else {
            return true
        }
    } else {
        return true;
    }
}