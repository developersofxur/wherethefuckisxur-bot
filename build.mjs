import * as esbuild from 'esbuild';

await esbuild.build({
    entryPoints: ['src/index.ts', 'src/bot.ts'],
    bundle: true,
    platform: 'node',
    target: 'node18',
    outdir: 'dist'
})